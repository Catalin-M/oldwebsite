package com.psyonik.website.entity;

/*
* The Post object holds information about a post
* `id` int unsigned NOT NULL auto_increment,
    `title` varchar(255),
    `author` int,
    `date` datetime NOT NULL,
    `excerpt` text NOT NULL,
    `featured_media` blob,
    `content` mediumtext NOT NULL,
    `category` varchar(255) NOT NULL,
* */

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;


@Entity
@Table(name="post")
public class Post {
    //fields of the Post object
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private int id;
    @Column(name="title")
    private String title;
    @Column(name="subtitle")
    private String subtitle;
    @Column(name="author")
    private String author;
    @DateTimeFormat(pattern="HH:mm dd-MM-yyyy")
    @Column(name="date")
    private LocalDateTime date;
    @Column(name="excerpt")
    private String excerpt;
    @Column(name="featured_media")
    private byte[] featuredMedia;
    @Column(name="content")
    private String content;
    @Column(name="category")
    private String category;



    //constructor for the Post object
    public Post(String title, String subtitle, String author, LocalDateTime date, String excerpt, byte[] featuredMedia, String content, String category) {
        this.title = title;
        this.subtitle = subtitle;
        this.author = author;
        this.date = date;
        this.excerpt = excerpt;
        this.featuredMedia = featuredMedia;
        this.content = content;
        this.category = category;
    }

    public Post() {}



    @Override
    public String toString() {
        return "Post{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", subtitle='" + subtitle + '\'' +
                ", author=" + author +
                ", date=" + date +
                ", excerpt='" + excerpt + '\'' +
                ", featuredMedia=" + Arrays.toString(featuredMedia) +
                ", content='" + content + '\'' +
                ", category='" + category + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getDate() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm dd-MM-yyyy");
        String formattedTime = date.format(formatter);
        return formattedTime;
    }

    public void setDate(String date) {
        if (date.length() > 17) {
            DateTimeFormatter longFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS");
            this.date = LocalDateTime.parse(date, longFormatter);
        }
        else {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm dd-MM-yyyy");
            this.date = LocalDateTime.parse(date, formatter);
        }
    }

    public String getExcerpt() {
        return excerpt;
    }

    public void setExcerpt(String excerpt) {
        this.excerpt = excerpt;
    }

    public byte[] getFeaturedMedia() {
        return featuredMedia;
    }

    public void setFeaturedMedia(byte[] featuredMedia) {
        this.featuredMedia = featuredMedia;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
