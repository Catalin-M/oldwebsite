package com.psyonik.website.controller;

import com.psyonik.website.dao.PostRepository;
import com.psyonik.website.entity.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Controller
public class PostController {
    private PostRepository postRepository;

    @Autowired
    public PostController(PostRepository postRepository) {
        this.postRepository = postRepository;
    }

    //mapping to load the homepage or index ==========================================================================//
    @RequestMapping("/")
    public String indexPage() {
        return "index";
    }

    //mapping to load the about page =================================================================================//
    @RequestMapping("/about")
    public String aboutPage() { return "about"; }


    //mapping to load the contact page ===============================================================================//
    @RequestMapping("/contact")
    public String contactPage() { return "contact"; }


    //mapping for the blog page itself ===============================================================================//
    @GetMapping("/posts")
    public String welcomeHome(@RequestParam(defaultValue = "0") int page, Model theModel) {
        Page<Post> posts = postRepository.findAll(PageRequest.of(page, 20, Sort.by("date").descending()));
        theModel.addAttribute("posts", posts);

        //this portion stores the total number of pages and stores this in pageNumbers and passes it on to the model
        //this is used for pagination purposes to create a list of posts
        int totalPages = posts.getTotalPages();
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(0, totalPages-1)
                    .boxed()
                    .collect(Collectors.toList());
            theModel.addAttribute("pageNumbers", pageNumbers);
        }

        return "Posts/post-list";
    }

    //this is the mapping that gets called when the add post button is pressed =======================================//
    @GetMapping("/posts/newPost")
    public String addNewPost(Model theModel) {
        //we use the model for data binding to be able to pass through the object properties
        Post newPost = new Post();  //the model attribute to bind the form data - the entity this form represents
        newPost.setDate(LocalDateTime.now().toString());
        theModel.addAttribute("post", newPost); //thymeleaf accesses this using the 'post' string
        return "Posts/postform";                  //this will look for src/main/resources/templates/posts/postform.html
    }

    //this is the mapping that gets called when the submit button is pressed in the postform.html file ===============//
    @PostMapping("/posts/saves")
    public String saveNewPost(@ModelAttribute("post") Post theNewPost) {
        postRepository.save(theNewPost);
        return "redirect:/posts";
    }

    //this is the mapping that gets called when the edit button is called ============================================//
    @GetMapping("/posts/editPost")
    public String showFormForEdit(@RequestParam("postId") int theId, Model theModel) {
        Post thePost;
        Optional<Post> tempPost = postRepository.findById(theId);
        if (!tempPost.isPresent()) {
            thePost = null;
            //no such post based on that id
        }
        else {
            thePost = tempPost.get();
            theModel.addAttribute(thePost);
        }
        return "Posts/postform";
    }

    //this is the mapping that gets called when the delete button is called ==========================================//
    @GetMapping("/posts/deletePost")
    public String deletePostById(@RequestParam("postId") int theId) {
        postRepository.deleteById(theId);
        return "redirect:/";
    }

    //this is the mapping that gets called when the read more button is called =======================================//
    @GetMapping("/posts/readPost")
    public String readPostById(@RequestParam("postId") int theId, Model theModel) {
        Post thePostToBeDisplayed;
        Optional<Post> tempPost = postRepository.findById(theId);   //this may or may not contain a post (depends if the id is found)
        if (!tempPost.isPresent()) {
            thePostToBeDisplayed = null;
            //no such post
        }
        else {
            thePostToBeDisplayed = tempPost.get();
        }
        theModel.addAttribute("post", thePostToBeDisplayed);
        return "Posts/displayPost";
    }

    //mapping to the login page ======================================================================================//
    @GetMapping("/login")
    public String login() {
        return "login";
    }

    //mapping to the privacy terms page ==============================================================================//
    @GetMapping("/privacy")
    public String privacy() {
        return "privacy-policy";
    }
}
