package com.psyonik.website.config;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import javax.sql.DataSource;
import java.util.logging.Logger;

@Configuration
@PropertySource("classpath:application.properties")     //path to the properties file
public class AppConfig {

    //new object that will hold the values from the properties file
    @Autowired
    private Environment env;

    //a logger copied from the video course to log the process
    //this is used to hold information that is being passed to and from the db
    private Logger logger = Logger.getLogger(getClass().getName());

    //define the bean that creates the DataSource object that will hold the information used for authenthication
    @Bean
    public DataSource securityDataSource() {
        //a new connection pool needs to be setup
        ComboPooledDataSource securityDataSource = new ComboPooledDataSource();

        //log the connection
        logger.info("JDBC url: " + env.getProperty("spring.datasource.url"));
        logger.info("JDBC user: " + env.getProperty("spring.datasource.username"));

        //set the database connection props
        securityDataSource.setJdbcUrl(env.getProperty("spring.datasource.url"));
        securityDataSource.setUser(env.getProperty("spring.datasource.username"));
        securityDataSource.setPassword(env.getProperty("spring.datasource.password"));

        //set the connection pool properties
        securityDataSource.setInitialPoolSize(getIntProperty("connection.pool.initialPoolSize"));
        securityDataSource.setMinPoolSize(getIntProperty("connection.pool.minPoolSize"));
        securityDataSource.setMaxPoolSize(getIntProperty("connection.pool.maxPoolSize"));
        securityDataSource.setMaxIdleTime(getIntProperty("connection.pool.maxIdleTime"));

        return securityDataSource;
    }

    //helper method that reads a property and converts it to an int
    private int getIntProperty(String property) {
        String propValue = env.getProperty(property);
        int intPropertyValue = Integer.parseInt(propValue);

        return intPropertyValue;
    }
}
