package com.psyonik.website.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.thymeleaf.extras.springsecurity5.dialect.SpringSecurityDialect;

import javax.sql.DataSource;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    //add reference to the data source
    @Autowired
    private DataSource securityDataSource;

    //this is used to enable the th:sec within the thymeleaf template
    @Bean
    public SpringSecurityDialect springSecurityDialect() {
        return new SpringSecurityDialect();
    }

    //configure entry points depending on roles
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/")
                .permitAll()
                .antMatchers("/posts/newPost")
                .hasRole("GOD")
                .antMatchers("/posts/editPost")
                .hasRole("GOD")
                .antMatchers("/posts/deletePost")
                .hasRole("GOD")
                .and()
                .formLogin()
                .loginPage("/login")
                .failureUrl("/login-error");
    }

    //in memory user to test this
    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        //use jdbc authentication
        auth.jdbcAuthentication().dataSource(securityDataSource);
    }
}
