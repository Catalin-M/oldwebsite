package com.psyonik.website.dao;

import com.psyonik.website.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

//the RepositoryResource changes the path from the pluralized version of the entity (users) to whatever the new path is
@RepositoryRestResource(path="blogusers")
public interface UserRepository extends JpaRepository<User, Integer> {
    //no implementation required
}
