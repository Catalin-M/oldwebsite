package com.psyonik.website.dao;

import com.psyonik.website.entity.Post;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;


public interface PostRepository extends JpaRepository<Post, Integer> {
    //no implementation required
    List<Post> findAllByDate(LocalDateTime date, Pageable pageable);
}
