This page is my old website that was running on Heroku.  

The page was build using Spring Boot, Thymeleaf, MySQL and Pure.css. It employed authentication, allowing for a user to log in, posts were written to a database and posts would be displayed from the database.  

It isn't very elegant, but provided my first usable CRUD app as I could create new posts, read posts by visiting the /posts endpoint, update older posts or even delete them.  

I had quite a bit of fun building it, but this has now been replaced by my Hugo based website that is in another repo on my account. That one will remain live while I find the time to put together another version :)  

To try and get it running for yourself, make sure to update the `application.properties` file with your DB connection string (I used MySQL so if you're using something else, you will need to update the connectors if I'm not mistaken), username and password for your DB. 